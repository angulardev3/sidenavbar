//javscript questions
const strR="This is Javascript Code";
const arrOfStr1=strR.split(' ');
const reverseStr=arrOfStr1.map(item => item.split('').reverse().join('')).join(' ')
console.log(reverseStr);

//maximum occurance in string
const str="JaJvasssscript";
const arrOfStr=str.split('');
const arr=arrOfStr.reduce((acc,curr)=>
Object.keys(acc).includes(curr)? {...acc,[curr.toLocaleLowerCase()]:
acc[curr]+1}:{...acc,[curr.toLocaleLowerCase()]:1},{});
console.log("Maximum num occur",arr);

// const str="This is Javscript code and you need to find max char";
function maxCharacter(strn){
    const arrOfStr=strn.split('')
    const charMap={};
    for(let char of arrOfStr){
        charMap[char]=charMap[char]+1 || 1;
    }
    // console.log("charmap",charMap)
    // for(let char in charMap){
    //     if(charMap[char]>max){
    //         max=charMap[char];
    //         maxChar=char;
    //     }
    // }
    return charMap;
}
console.log("maximummethod-2",maxCharacter("JaJvasssscript"));